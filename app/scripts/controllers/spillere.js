'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:SpillereCtrl
 * @description
 * # SpillereCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('SpillereCtrl', function ($scope, $log, $rootScope, SpillerService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    if(!$scope.spillereRank) {
        SpillerService.hentRank();
    }

    if(!$scope.valgtSpiller) {
        $rootScope.valgtSpiller = {};
    }

    if($scope.valgtSpiller.description != null) {
        SpillerService.hentSpiller($scope.valgtSpiller.description.Navn);
    }

    // Dersom spillere-viewet åpnes og det ikke er valgt en spiller, hentes en tilfeldig
    $scope.$watch('spillereRank', function() {
        if($scope.valgtSpiller.description == null) {
            $rootScope.valgtSpiller.description = $scope.spillereRank[Math.floor(Math.random() * $scope.spillereRank.length)];
            SpillerService.hentSpiller($scope.valgtSpiller.description.Navn);
        }
    });

    // Dersom spiller1 endres og vi har en spiller2, oppdateres kampsannsynligheten
    $scope.$watch('valgtSpiller', function() {
        SpillerService.hentSpiller($scope.valgtSpiller.description.Navn);
        if($scope.spiller2.description != null) {
            SpillerService.hentKampSanns($scope.valgtSpiller.description.Navn, $scope.spiller2.description.Navn);
        }
    }, true);

    // Dersom spiller2 endres og vi har en spiller1, oppdateres kampsannsynligheten
    $scope.$watch('spiller2', function() {
        if($scope.valgtSpiller.description != null) {
            SpillerService.hentSpiller2($scope.spiller2.description.Navn);
            SpillerService.hentKampSanns($scope.valgtSpiller.description.Navn, $scope.spiller2.description.Navn);
        }
    }, true);

    $scope.tiSiste = function() {
        SpillerService.seTiSiste();
    };

    $scope.seAlle = function() {
        SpillerService.seAlle();
    };

  });
