'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:SesongerCtrl
 * @description
 * # SesongerCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('SesongerCtrl', function ($scope, $log, sesongservice) {

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var hentSesongHistorikk = function() {
        sesongservice.hentSesongHistorikk();
    };

    if(!$scope.sesongHistorikkTabell) {
        hentSesongHistorikk();
    }

    $scope.limit = 10;

    $scope.incrementLimit = function() {
        if($scope.limit < $scope.sesongHistorikkTabell.length) {
            $scope.limit += 10;
        }
    };

    $scope.decrementLimit = function() {
        if($scope.limit > 10) {
            $scope.limit -= 10;
        }
    };

    $scope.maxLimit = function() {
        if($scope.limit == $scope.sesongHistorikkTabell.length) {
            $scope.limit = 10;
        } else {
            $scope.limit = $scope.sesongHistorikkTabell.length;
        }
    };

  });
