'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('HeaderCtrl', function ($scope, $location) {

      $scope.isActive = function(viewLocation) {
          return viewLocation === $location.path();
      };

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
