'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
