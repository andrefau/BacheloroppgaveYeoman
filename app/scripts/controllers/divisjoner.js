'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:DivisjonerCtrl
 * @description
 * # DivisjonerCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('DivisjonerCtrl', function ($scope, $log, DivisjonService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var fyllSesongerTab = function() {
        var tab = [];
        for (var i = 112; i > 0; i--) {
            var obj = {value: i, label: i};
            tab.push(obj);
        }
        return tab;
    };

    $scope.sesonger = fyllSesongerTab();

    var hentDivisjonHistorikk = function() {
        DivisjonService.hentDivisjonHistorikk();
    };

    if(!$scope.divisjonHistorikk) {
        hentDivisjonHistorikk();
    }

    $scope.hentDiagrammer = function() {
        $log.info("Du valgte sesong " +$scope.valgtSesong.value);
        DivisjonService.visValgtSesong($scope.valgtSesong.value-1);
        $scope.divisjonHistorikkTabell = $scope.divisjonHistorikk[$scope.valgtSesong.value-1].divisjoner;
    };

  });
