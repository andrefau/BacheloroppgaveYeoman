'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:GrupperCtrl
 * @description
 * # GrupperCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
  .controller('GrupperCtrl', function ($scope, $log, GruppeService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var fyllSesongerTab = function() {
        var tab = [];
        for (var i = 112; i > 0; i--) {
            var obj = {value: i, label: i};
            tab.push(obj);
        }
        return tab;
    };

    $scope.sesonger = fyllSesongerTab();

    $scope.oppdaterDivisjoner = function() {
        $log.info("oppdaterDivisjoner() kalt");
        var tab = [];
        for (var i = 1; i <= $scope.gruppeHistorikk[$scope.valgtSesong.value-1].divisjoner.length; i++) {
            var obj = {value: i, label: i};
            tab.push(obj);
        }
        $scope.divisjoner = tab;
    };

    var hentGruppeHistorikk = function() {
        GruppeService.hentGruppeHistorikk();
    };

    if(!$scope.gruppeHistorikk) {
        hentGruppeHistorikk();
    }

    $scope.hentDiagrammer = function() {
        $log.info("Du valgte sesong " +$scope.valgtSesong.value+ " og divisjon " +$scope.valgtDivisjon.value);
        GruppeService.visValgtDivisjon($scope.valgtSesong.value-1, $scope.valgtDivisjon.value-1);
        $scope.gruppeHistorikkTabell = JSON.parse($scope.gruppeHistorikk[$scope.valgtSesong.value-1].divisjoner[$scope.valgtDivisjon.value-1].grupper);
        $scope.limit = 10;
    };

    $scope.incrementLimit = function() {
        if($scope.limit < $scope.gruppeHistorikkTabell.length) {
            $scope.limit += 10;
        }
    };

    $scope.decrementLimit = function() {
        if($scope.limit > 10) {
            $scope.limit -= 10;
        }
    };

    $scope.maxLimit = function() {
        if($scope.limit == $scope.gruppeHistorikkTabell.length) {
            $scope.limit = 10;
        } else {
            $scope.limit = $scope.gruppeHistorikkTabell.length;
        }
    };

  });
