'use strict';

/**
 * @ngdoc function
 * @name wlohApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wlohApp
 */
angular.module('wlohApp')
    .controller('MainCtrl', function($scope, $rootScope, SpillerService) {
        this.tittel = "Startside";

        var hentRank = function() {
            SpillerService.hentRank();
        };

        if(!$scope.spillereRank) {
            hentRank();
        }

        if(!$scope.valgtSpiller) {
            $rootScope.valgtSpiller = {};
        }
    });
