'use strict';

/**
 * @ngdoc overview
 * @name wlohApp
 * @description
 * # wlohApp
 *
 * Main module of the application.
 */
angular
  .module('wlohApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'googlechart',              // Hentet fra https://github.com/angular-google-chart/angular-google-chart
    'angular-loading-bar',      // Hentet fra https://github.com/chieffancypants/angular-loading-bar
    'angucomplete-alt'          // Hentet fra https://github.com/ghiden/angucomplete-alt
  ])
  .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/sesonger', {
        templateUrl: 'views/sesonger.html',
        controller: 'SesongerCtrl',
        controllerAs: 'sesonger'
      })
      .when('/divisjoner', {
        templateUrl: 'views/divisjoner.html',
        controller: 'DivisjonerCtrl',
        controllerAs: 'divisjoner'
      })
      .when('/grupper', {
        templateUrl: 'views/grupper.html',
        controller: 'GrupperCtrl',
        controllerAs: 'grupper'
      })
      .when('/spillere', {
        templateUrl: 'views/spillere.html',
        controller: 'SpillereCtrl',
        controllerAs: 'spillere'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
