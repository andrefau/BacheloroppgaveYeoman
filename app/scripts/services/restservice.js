'use strict';

/**
 * @ngdoc service
 * @name wlohApp.RestService
 * @description
 * # RestService
 * Service in the wlohApp.
 */
angular.module('wlohApp')
  .service('RestService', function($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    // Kode for å hente alle spillere med rank
    var getRanking = function() {
      return $http.get("http://82.196.1.230:8080/wloh/api/ranking") // Remote server
        .then(function(response) {
          return response.data;
        });
    };

    // Kode for å hente generell statistikk for hver sesong
    var getStatistikk = function() {
        return $http.get("http://82.196.1.230:8080/wloh/api/statistikk")    // Remote server
            .then(function(response) {
                return response.data;
            });
    };

    // Kode for å hente info om en spiller
    var getSpillerInfo = function(navn) {
        return $http.get("http://82.196.1.230:8080/wloh/api/spillere/" +navn)     // Remote server
            .then(function(response) {
                return response.data;
            });
    };

    // Kode for å hente sannsynlighet for at en spiller
    // vinner over en annen spiller
    var getKampSanns = function(navn1, navn2) {
        return $http.get("http://82.196.1.230:8080/wloh/api/vinnersannsynlighet/" + navn1 + "/" + navn2)  // Remote server
        .then(function(response) {
          return response.data;
        });
    };

    // Kode for å hente sannsynlighet for at en spiller
    // vinner i sin gruppe
    var getGruppeSanns = function(navn) {
        return $http.get("http://82.196.1.230:8080/wloh/api/sannsynlighetgruppeseier/" + navn)    // Remote server
        .then(function(response) {
          return response.data;
        });
    };

    return {
      getRanking: getRanking,
      getStatistikk: getStatistikk,
      getSpillerInfo: getSpillerInfo,
      getKampSanns: getKampSanns,
      getGruppeSanns: getGruppeSanns
    };
  });
