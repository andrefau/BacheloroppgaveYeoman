'use strict';

/**
 * @ngdoc service
 * @name wlohApp.GruppeService
 * @description
 * # GruppeService
 * Service in the wlohApp.
 */
angular.module('wlohApp')
  .service('GruppeService', function ($rootScope, RestService, googleChartApiPromise) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var lagAntallWFPoengPerGruppeString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Gruppe","type":"string"},' +
        '{"label":"Antall wordfeudpoeng","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
          var snitt = data[i].antall_wfpoeng / data[i].antall_spillere;
        string += '{"c":[{"v":"' + data[i].gruppe + '"},{"v":' + snitt + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var onError = function(reason) {
      $rootScope.grupperserviceError = "Kunne ikke finne dataene.";
    };

    var onGruppeHistorikk = function(data) {
        $rootScope.gruppeHistorikk = data;
    };

    var hentGruppeHistorikk = function() {
        RestService.getStatistikk().then(onGruppeHistorikk, onError);
    };

    var visValgtDivisjon = function(sesong, divisjon) {
        $rootScope.antallWFPoengPerGruppe = lagAntallWFPoengPerGruppeString(JSON.parse($rootScope.gruppeHistorikk[sesong].divisjoner[divisjon].grupper));
        googleChartApiPromise.then(buildGruppeHistorikkTables);
    };

    /* -------- Antall wfpoeng per gruppe -------- */
    $rootScope.histogramAntallWFPoengPerGruppe = {
      type: "Histogram",
      options: {
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall grupper per Wordfeudpoeng-intervall",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Antall Wordfeudpoeng",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall grupper",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.columnChartAntallWFPoengPerGruppe = {
      type: "ColumnChart",
      options: {
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall Wordfeudpoeng per gruppe",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Gruppe",
          maxAlternation: 1,
          slantedText: false,
          baselineColor: 'black'
        },
        vAxis: {
          title: "Antall Wordfeudpoeng",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    function buildGruppeHistorikkTables() {
        var antWFPoeng = JSON.parse($rootScope.antallWFPoengPerGruppe);

        $rootScope.histogramAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
        $rootScope.columnChartAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
    };

    return {
        hentGruppeHistorikk: hentGruppeHistorikk,
        visValgtDivisjon: visValgtDivisjon
    };

  });
