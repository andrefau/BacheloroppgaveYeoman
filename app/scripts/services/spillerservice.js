'use strict';

/**
 * @ngdoc service
 * @name wlohApp.SpillerService
 * @description
 * # SpillerService
 * Service in the wlohApp.
 */
angular.module('wlohApp')
  .service('SpillerService', function ($rootScope, $location, $log, RestService, googleChartApiPromise) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var lagStringRanking = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"rating", "label":"Rating", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Rank + '},null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringRankingTiSiste = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"rating", "label":"Rating", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      for (var i = data.length-10; i < data.length; i++) {
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Rank + '},null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultater = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
        '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}},' +
        '{"id":"uavgjort", "label":"Uavgjort", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Vunnet +
          '},{"v":' + data[i].Tapt + '},{"v":' + data[i].Uavgjort + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultaterTiSiste = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
        '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}},' +
        '{"id":"uavgjort", "label":"Uavgjort", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var start = data.length - 10;
      var teller = data[start].Sesong;
      for (var i = start; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Vunnet +
          '},{"v":' + data[i].Tapt + '},{"v":' + data[i].Uavgjort + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var seTiSiste = function() {
        $rootScope.antSesongerBeskjed = "Viser ti siste spilte sesonger";
        $rootScope.tiSisteVises = true;
        if ($rootScope.spillerFunnet.Resultater.length > 10) {
            $rootScope.spillerRanking = lagStringRankingTiSiste(JSON.parse($rootScope.spillerFunnet.Ranking));
            $rootScope.resultatString = lagStringKampResultaterTiSiste($rootScope.spillerFunnet.Resultater);
            googleChartApiPromise.then(buildSpillerTables);
        }

    };

    var seAlle = function() {
        $rootScope.antSesongerBeskjed = "Viser alle spilte sesonger";
        $rootScope.tiSisteVises = false;
        $rootScope.spillerRanking = lagStringRanking(JSON.parse($rootScope.spillerFunnet.Ranking));
        $rootScope.resultatString = lagStringKampResultater($rootScope.spillerFunnet.Resultater);
        googleChartApiPromise.then(buildSpillerTables);
    };

    var onRanking = function(data) {
        $rootScope.spillereRank = data;
    };

    var onSpillerFunnet = function(data) {
        $rootScope.spillerFunnet = data;
        if (data.Resultater.length > 10) {
            seTiSiste();
        } else {
            seAlle();
        }

    };

    var onSpiller2Funnet = function(data) {
        $rootScope.spiller2Funnet = data;
    };

    var onKampSanns = function(data) {
        $rootScope.kampSanns = data;
    };

    var onError = function(reason) {
        $rootScope.spillerserviceError = "Kunne ikke finne spilleren(e)";
    };

    var hentRank = function() {
        RestService.getRanking().then(onRanking, onError);
    };

    var hentSpiller = function(navn) {
        RestService.getSpillerInfo(navn).then(onSpillerFunnet, onError);
    };

    var hentSpiller2 = function(navn) {
        RestService.getSpillerInfo(navn).then(onSpiller2Funnet, onError);
    };

    var hentKampSanns = function(navn1, navn2) {
        RestService.getKampSanns(navn1, navn2).then(onKampSanns, onError);
    };

    $rootScope.$watch(function() {
        return $location.path();
    }, function(value) {
        if(value != "/spillere") {
            $rootScope.kampSanns = null;
        }
    });

    $rootScope.areaChartSpillerRanking = {
      type: "AreaChart",
      options: {
        areaOpacity: 1.0,
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Rating per sesong",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.stackedColumnChartSpillerSoek = {
      type: "ColumnChart",
      options: {
        isStacked: true,
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    function buildSpillerTables() {
        var ranking = JSON.parse($rootScope.spillerRanking);
        var resultater = JSON.parse($rootScope.resultatString);
        $rootScope.areaChartSpillerRanking.data = new google.visualization.DataTable(ranking);
        $rootScope.stackedColumnChartSpillerSoek.data = new google.visualization.DataTable(resultater);
    };

    return {
        hentRank: hentRank,
        hentSpiller: hentSpiller,
        hentSpiller2: hentSpiller2,
        hentKampSanns: hentKampSanns,
        seTiSiste: seTiSiste,
        seAlle: seAlle
    };

  });
