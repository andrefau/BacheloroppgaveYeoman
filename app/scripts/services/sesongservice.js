'use strict';

/**
 * @ngdoc service
 * @name wlohApp.sesongservice
 * @description
 * # sesongservice
 * Service in the wlohApp.
 */
angular.module('wlohApp')
  .service('sesongservice', function($rootScope, RestService, googleChartApiPromise) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var lagAntallSpillerePerSesongString = function(data) {
        var string = '{' +
              '"cols": [' +
                    '{"label":"Sesong","type":"string"},' +
                    '{"label":"Antall spillere","type":"number"}' +
                  '],' +
             ' "rows": [';
        for (var i in data) {
            string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_spillere+ '}, null]},';
        }
        string = string.slice(0, -1);
        string += ']}';
        return string;
    };

    var lagAntallUavgjortPerSesongString = function(data) {
        var string = '{' +
              '"cols": [' +
                    '{"label":"Sesong","type":"string"},' +
                    '{"label":"Antall uavgjort","type":"number"}' +
                  '],' +
             ' "rows": [';
        for (var i in data) {
            string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_uavgjort+ '}]},';
        }
        string = string.slice(0, -1);
        string += ']}';
        return string;
    };

    var lagAntallWFPoengPrSesongString = function(data) {
        var string = '{' +
              '"cols": [' +
                    '{"label":"Sesong","type":"string"},' +
                    '{"label":"Antall wordfeudpoeng","type":"number"}' +
                  '],' +
             ' "rows": [';
        for (var i in data) {
            string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_wfpoeng+ '}]},';
        }
        string = string.slice(0, -1);
        string += ']}';
        return string;
    };

    var onError = function(reason) {
        $rootScope.sesongserviceError = "Kunne ikke finne dataene";
    };

    var onSesongHistorikk = function(data) {
        $rootScope.sesongHistorikkTabell = data;
        $rootScope.antallSpillerePerSesong = lagAntallSpillerePerSesongString(data);
        $rootScope.antallUavgjortPerSesong = lagAntallUavgjortPerSesongString(data);
        $rootScope.antallWFPoengPerSesong = lagAntallWFPoengPrSesongString(data);

        googleChartApiPromise.then(buildSesongHistorikkTables);
    };

    var hentSesongHistorikk = function() {
        RestService.getStatistikk().then(onSesongHistorikk, onError);
    };

    $rootScope.areaChartAntallSpillerePerSesong = {
      type: "AreaChart",
      options: {
        areaOpacity: 1.0,
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall spillere per sesong",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
            title: "Sesong",
            maxAlternation: 1,
            slantedText: false
        },
        vAxis: {
            gridlines: {
              color: '#f2f2f2'
            }
        }
      }
    };

    $rootScope.areaChartAntallUavgjortPerSesong = {
      type: "AreaChart",
      options: {
        areaOpacity: 1.0,
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall uavgjort per sesong",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
            title: "Sesong",
            maxAlternation: 1,
            slantedText: false
        },
        vAxis: {
            gridlines: {
              color: '#f2f2f2'
            }
        }
      }
    };

    $rootScope.areaChartAntallWFPoengPerSesong = {
        type: "AreaChart",
        options: {
          areaOpacity: 1.0,
          colors: ['#3679b0'],
          backgroundColor: {
            fill: '#f2f2f2'
          },
          legend: {
            position: 'none'
          },
            title: "Antall Wordfeudpoeng per sesong",
            titleTextStyle: {
              bold: 'false',
              fontSize: 11
            },
            hAxis: {
                title: "Sesong",
                maxAlternation: 1,
                slantedText: false
            },
            vAxis: {
                gridlines: {
                  color: '#f2f2f2'
                }
            }
        }
    };

    function buildSesongHistorikkTables() {
        var antSpillere = JSON.parse($rootScope.antallSpillerePerSesong);
        var antUavgjort = JSON.parse($rootScope.antallUavgjortPerSesong);
        var antWFPoeng = JSON.parse($rootScope.antallWFPoengPerSesong);

        $rootScope.areaChartAntallSpillerePerSesong.data = new google.visualization.DataTable(antSpillere);
        $rootScope.areaChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(antUavgjort);
        $rootScope.areaChartAntallWFPoengPerSesong.data = new google.visualization.DataTable(antWFPoeng);
    };

    return {
        hentSesongHistorikk: hentSesongHistorikk
    };

  });
