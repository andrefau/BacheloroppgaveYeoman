'use strict';

/**
 * @ngdoc service
 * @name wlohApp.DivisjonService
 * @description
 * # DivisjonService
 * Service in the wlohApp.
 */
angular.module('wlohApp')
  .service('DivisjonService', function ($rootScope, RestService, googleChartApiPromise) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var lagAntallSpillerePerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall spillere","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_spillere + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallWFPoengPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall wordfeudpoeng","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_wfpoeng + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallUavgjortPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall uavgjort","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_uavgjort + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var onError = function(reason) {
        $rootScope.divisjonserviceError = "Kunne ikke finne dataene";
    };

    var onDivisjonHistorikk = function(data) {
        $rootScope.divisjonHistorikk = data;
    };

    var hentDivisjonHistorikk = function() {
        RestService.getStatistikk().then(onDivisjonHistorikk, onError);
    };

    var visValgtSesong = function(sesong) {
        $rootScope.antallSpillerePerDivisjon = lagAntallSpillerePerDivisjonString($rootScope.divisjonHistorikk[sesong].divisjoner);
        $rootScope.antallUavgjortPerDivisjon = lagAntallUavgjortPerDivisjonString($rootScope.divisjonHistorikk[sesong].divisjoner);
        $rootScope.antallWFPoengPerDivisjon = lagAntallWFPoengPerDivisjonString($rootScope.divisjonHistorikk[sesong].divisjoner);

        googleChartApiPromise.then(buildDivisjonHistorikkTables);
    };

    $rootScope.columnChartAntallSpillerePerDivisjon = {
      type: "ColumnChart",
      options: {
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall spillere per divisjon",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Divisjon",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.columnChartAntallWFPoengPerDivisjon = {
      type: "ColumnChart",
      options: {
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall Wordfeudpoeng per divisjon",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Divisjon",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.columnChartAntallUavgjortPerDivisjon = {
      type: "ColumnChart",
      options: {
        colors: ['#3679b0'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        title: "Antall uavgjort per divisjon",
        titleTextStyle: {
          bold: 'false',
          fontSize: 11
        },
        hAxis: {
          title: "Divisjon",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    function buildDivisjonHistorikkTables() {
        var antSpillere = JSON.parse($rootScope.antallSpillerePerDivisjon);
        var antUavgjort = JSON.parse($rootScope.antallUavgjortPerDivisjon);
        var antWFPoeng = JSON.parse($rootScope.antallWFPoengPerDivisjon);

        $rootScope.columnChartAntallSpillerePerDivisjon.data = new google.visualization.DataTable(antSpillere);
        $rootScope.columnChartAntallUavgjortPerDivisjon.data = new google.visualization.DataTable(antUavgjort);
        $rootScope.columnChartAntallWFPoengPerDivisjon.data = new google.visualization.DataTable(antWFPoeng);
    };

    return {
        hentDivisjonHistorikk: hentDivisjonHistorikk,
        visValgtSesong: visValgtSesong
    };

  });
