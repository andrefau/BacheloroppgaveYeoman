'use strict';

describe('Service: SpillerService', function () {

  // load the service's module
  beforeEach(module('wlohApp'));

  // instantiate service
  var SpillerService;
  beforeEach(inject(function (_SpillerService_) {
    SpillerService = _SpillerService_;
  }));

  it('should do something', function () {
    expect(!!SpillerService).toBe(true);
  });

});
