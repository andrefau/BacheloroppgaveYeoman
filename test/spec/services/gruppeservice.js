'use strict';

describe('Service: GruppeService', function () {

  // load the service's module
  beforeEach(module('wlohApp'));

  // instantiate service
  var GruppeService;
  beforeEach(inject(function (_GruppeService_) {
    GruppeService = _GruppeService_;
  }));

  it('should do something', function () {
    expect(!!GruppeService).toBe(true);
  });

});
