'use strict';

describe('Service: RestService', function () {

  // load the service's module
  beforeEach(module('wlohApp'));

  // instantiate service
  var RestService;
  var httpBackend;

  beforeEach(inject(function (_RestService_, $httpBackend) {
    RestService = _RestService_;
    httpBackend = $httpBackend;
  }));

    it('skal returnere rank topp 3', function() {
        httpBackend.whenGET("http://82.196.1.230:8080/wloh/api/ranking").respond(
            [
                {
                    "Navn":"rubenremus",
                    "TotaltSpilteSesonger":"94",
                    "Rank":1,"Rating":"5283.3638257977445"
                },
                {
                    "Navn":"Tommoen",
                    "TotaltSpilteSesonger":"88",
                    "Rank":2,
                    "Rating":"5249.357220302771"
                },
                {
                    "Navn":"harryhotspur",
                    "TotaltSpilteSesonger":"74",
                    "Rank":3,
                    "Rating":"5236.415680500063"
                }
            ]
        );
        httpBackend.whenGET("views/main.html").respond(200);
        RestService.getRanking().then(function(data) {
            expect(data.length).toBe(3);
            expect(data[1].Navn).toEqual("Tommoen");
        });
        httpBackend.flush();
    });

    it('skal returnere generell statistikk', function() {
        httpBackend.whenGET("http://82.196.1.230:8080/wloh/api/statistikk").respond(
            [
                {
                    "sesong":4,
                    "antall_divisjoner":2,
                    "antall_grupper":2,
                    "antall_spillere":19,
                    "antall_uavgjort":0,
                    "antall_wfpoeng":53393,
                    "divisjoner":[
                        {
                            "divisjon":1,
                            "antall_grupper":1,
                            "antall_spillere":9,
                            "antall_wfpoeng":33548,
                            "antall_uavgjort":0,
                            "grupper":[
                                {
                                    "gruppe": 1,
                                    "antall_spillere": 9,
                                    "antall_wfpoeng": 33548,
                                    "antall_uavgjort": 0
                                }
                            ]
                        },
                        {
                            "divisjon":2,
                            "antall_grupper":1,
                            "antall_spillere":10,
                            "antall_wfpoeng":19845,
                            "antall_uavgjort":0,
                            "grupper":[
                                {
                                    "gruppe": 1,
                                    "antall_spillere": 10,
                                    "antall_wfpoeng": 19845,
                                    "antall_uavgjort": 0
                                }
                            ]
                        }
                    ]
                }
            ]
        );
        httpBackend.whenGET("views/main.html").respond(200);
        RestService.getStatistikk().then(function(data) {
            expect(data.length).toBe(1);
            expect(data[0].divisjoner.length).toBe(2);
            expect(data[0].divisjoner[1].grupper[0].antall_wfpoeng).toBe(19845);
        });
        httpBackend.flush();
    });

    it('skal returnere spillerinfo', function() {
        httpBackend.whenGET("http://82.196.1.230:8080/wloh/api/spillere/Tommoen").respond(
            {
                "Navn":"Tommoen",
                "Url":"http://leagues.aasmul.net/bruker-1449",
                "Sted":"Årdal",
                "Fodt":"1956",
                "Ranking":[
                    {
                        "Sesong": 16,
                        "Rank": 19.5
                    },
                    {
                        "Sesong": 17,
                        "Rank": 112.41666666666671
                    }
                ]
            }
        );
        httpBackend.whenGET("views/main.html").respond(200);
        RestService.getSpillerInfo("Tommoen").then(function(data) {
            expect(data.Navn).toEqual("Tommoen");
            expect(data.Ranking.length).toBe(2);
            expect(data.Ranking[0].Rank).toBe(19.5);
        });
        httpBackend.flush();
    });

    it('skal returnere kampsannsynlighet', function() {
        httpBackend.whenGET("http://82.196.1.230:8080/wloh/api/vinnersannsynlighet/Tommoen/skwais").respond(
            {
                "Spiller1": "Tommoen",
                "Spiller2": "skwais",
                "Sannsynlighet": 62.56,
                "Sesong": 112
            }
        );
        httpBackend.whenGET("views/main.html").respond(200);
        RestService.getKampSanns("Tommoen", "skwais").then(function(data) {
            expect(data.Spiller1).toEqual("Tommoen");
            expect(data.Spiller2).toEqual("skwais");
            expect(data.Sannsynlighet).toBe(62.56);
            expect(data.Sesong).toBe(112);
        });
        httpBackend.flush();
    });

    it('skal returnere gruppesannsynlighet', function() {
        httpBackend.whenGET("http://82.196.1.230:8080/wloh/api/sannsynlighetgruppeseier/Tommoen").respond(
            {
                "Navn": "Tommoen",
                "Sannsynlighet": 8.23,
                "Sesong": 112
            }
        );
        httpBackend.whenGET("views/main.html").respond(200);
        RestService.getGruppeSanns("Tommoen").then(function(data) {
            expect(data.Navn).toEqual("Tommoen");
            expect(data.Sannsynlighet).toBe(8.23);
            expect(data.Sesong).toBe(112);
        });
        httpBackend.flush();
    });

});
