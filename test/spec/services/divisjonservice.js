'use strict';

describe('Service: DivisjonService', function () {

  // load the service's module
  beforeEach(module('wlohApp'));

  // instantiate service
  var DivisjonService;
  beforeEach(inject(function (_DivisjonService_) {
    DivisjonService = _DivisjonService_;
  }));

  it('should do something', function () {
    expect(!!DivisjonService).toBe(true);
  });

});
