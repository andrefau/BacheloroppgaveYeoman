'use strict';

describe('Service: sesongservice', function () {

  // load the service's module
  beforeEach(module('wlohApp'));

  // instantiate service
  var sesongservice;
  beforeEach(inject(function (_sesongservice_) {
    sesongservice = _sesongservice_;
  }));

  it('should do something', function () {
    expect(!!sesongservice).toBe(true);
  });

});
