'use strict';

describe('Controller: SpillereCtrl', function () {

  // load the controller's module
  beforeEach(module('wlohApp'));

  var SpillereCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SpillereCtrl = $controller('SpillereCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SpillereCtrl.awesomeThings.length).toBe(3);
  });
});
