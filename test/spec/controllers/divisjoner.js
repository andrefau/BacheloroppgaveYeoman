'use strict';

describe('Controller: DivisjonerCtrl', function() {

  // load the controller's module
  beforeEach(module('wlohApp'));

  var DivisjonerCtrl;
  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    DivisjonerCtrl = $controller('DivisjonerCtrl', {
      $scope: scope
        // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(DivisjonerCtrl.awesomeThings.length).toBe(3);
  });
});
