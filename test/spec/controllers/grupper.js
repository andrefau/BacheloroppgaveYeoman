'use strict';

describe('Controller: GrupperCtrl', function () {

  // load the controller's module
  beforeEach(module('wlohApp'));

  var GrupperCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GrupperCtrl = $controller('GrupperCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GrupperCtrl.awesomeThings.length).toBe(3);
  });
});
