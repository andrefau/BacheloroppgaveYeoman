'use strict';

describe('Controller: SesongerCtrl', function () {

  // load the controller's module
  beforeEach(module('wlohApp'));

  var SesongerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SesongerCtrl = $controller('SesongerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SesongerCtrl.awesomeThings.length).toBe(3);
  });
});
